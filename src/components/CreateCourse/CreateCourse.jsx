import React, { useCallback, useState } from 'react';
import './CreateCourse.style.css';
import { Button } from '../common/Button';
import { Input } from '../common/Input';
import { TextArea } from '../common/TextArea';
import { CreateOptions } from './components/CreateOptions';
import { CoursesContext } from '../Storage/CoursesStorage/CoursesStorage';
import * as uuid from 'uuid';

export function CreateCourse(props) {
	const { setCourses, courses } = React.useContext(CoursesContext);
	const [title, setTitle] = useState('');
	const [description, setDescription] = useState('');
	const [duration, setDuration] = useState();
	const [authors, setAuthors] = useState([]);

	const createCourse = useCallback(
		() => {
			if (!title | !description | !duration | !authors) {
				alert('Please,fill on all fills');
			}
			const newId = uuid.v4();
			const date = new Date().toLocaleDateString();
			setCourses([
				...courses,
				{
					id: newId,
					title,
					description,
					duration,
					authors,
					creationDate: date,
				},
			]);
			console.log(courses);
			props.onCloseCourseClick();
		}, // eslint-disable-next-line
		[courses,setCourses, title,description,duration,authors]
	);

	const handleTitleChange = useCallback(
		(title) => {
			setTitle(title);
			// console.log(title);
		}, // eslint-disable-next-line
		[courses]
	);
	const handleDescriptionChange = useCallback(
		(description) => {
			setDescription(description);

			// console.log(description);
		}, // eslint-disable-next-line
		[description]
	);

	const handleDuration = useCallback(
		(duration) => {
			setDuration(+duration);

			// console.log(duration);
		}, // eslint-disable-next-line
		[duration]
	);

	const handleCoureseAuthor = useCallback(
		(author) => {
			setAuthors([...authors, author]);
		}, // eslint-disable-next-line
		[authors]
	);

	return (
		<div className='create-container'>
			<div className='create-course-container'>
				<div className='title-container'>
					<Input
						onChange={handleTitleChange}
						value={title}
						placeholder='Enter course title'
						label='Title:'
						id='courseTitle'
						type='text'
					/>

					<Button onClick={createCourse}>Create Corse</Button>
				</div>
				<TextArea
					value={courses.description}
					onChange={handleDescriptionChange}
					label='Description:'
					name='courseCreateDescription'
					id='courseCreateDescription'
					placeholder='Enter course description'
				/>
			</div>
			<CreateOptions
				onAddDuration={handleDuration}
				setAuthor={handleCoureseAuthor}
			/>
		</div>
	);
}
