import React, { useCallback } from 'react';
import './AuthorsList.style.css';
import { Button } from '../../../common/Button';

export function AuthorsList(props) {
	const handleClick = useCallback(
		(e) => {
			props.onClick(e.target.value);

			// eslint-disable-next-line
	}, [props.onClick]);

	return (
		<div className='options-item'>
			<h2>Authors</h2>
			<div className='author-list-container'>
				{props.authors.map((author) => (
					<div className='author-item' key={author.id}>
						<p className='author-name'>{author.name}</p>
						<div className='author-item-button'>
							<Button value={author.id} onClick={handleClick}>
								Add Author
							</Button>
						</div>
					</div>
				))}
			</div>
		</div>
	);
}
