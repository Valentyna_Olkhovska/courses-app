import React, { useCallback, useState } from 'react';
import './AddAuthor.style.css';
import { Button } from '../../../common/Button';
import { Input } from '../../../common/Input';

export function AddAuthor(props) {
	const [value, setValue] = useState('');

	const handleClick = useCallback(() => {
		props.onAdd(value);
		// eslint-disable-next-line
	}, [ value]);

	return (
		<div className='options-item'>
			<h2>Add Author</h2>

			<Input
				onChange={setValue}
				value={value}
				placeholder='Enter author name...'
				label='Author name:'
				id='newAuthorName'
				type='text'
			/>
			<Button onClick={handleClick}>Create author</Button>
		</div>
	);
}
