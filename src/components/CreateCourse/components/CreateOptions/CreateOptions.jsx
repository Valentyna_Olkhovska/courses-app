import React, { useCallback, useState } from 'react';
import * as uuid from 'uuid';
import './CreateOptions.style.css';
import { AddAuthor } from '../AddAuthor';
import { DurationCourse } from '../DurationCourse';
import { AuthorsList } from '../AuthorsList';
import { CourseAuthors } from '../CourseAuthors';
import { AuhtorsContext } from '../../../Storage/AuhtorsStorage';

export function CreateOptions(props) {
	const { setAuthors, authors } = React.useContext(AuhtorsContext);
	const [courseAuthors, setCourseAuthors] = useState([]);
	const [authorList, setAuthorList] = useState(authors);

	const handleAddAuthor = useCallback(
		(value) => {
			const newId = uuid.v4();
			setAuthors([...authors, { id: newId, name: value }]);
			setAuthorList([...authorList, { id: newId, name: value }]);
			// eslint-disable-next-line
	}, [authors,authorList]);

	const handleAddCourseAuthor = useCallback(
		(id) => {
			const currentAuthor = authors.find((author) => {
				return author.id === id;
			});

			setCourseAuthors([...courseAuthors, currentAuthor]);
			props.setAuthor(currentAuthor.id);

			setAuthorList(authorList.filter((author) => author.id !== id));
			// eslint-disable-next-line
	}, [courseAuthors,authors,authorList]);

	const handleDelCourseAuthor = useCallback(
		(id) => {
			const currentAuthor = authors.find((author) => {
				return author.id === id;
			});
			setCourseAuthors(courseAuthors.filter((author) => author.id !== id));

			setAuthorList([...authorList, currentAuthor]);
			// eslint-disable-next-line
	}, [authorList,courseAuthors,authors]);

	const handleDuration = useCallback((duration) => {
		props.onAddDuration(duration);
		// eslint-disable-next-line
	}, []);

	return (
		<div className='create-options-container'>
			<div className='options-container'>
				<AddAuthor onAdd={handleAddAuthor} />
				<AuthorsList onClick={handleAddCourseAuthor} authors={authorList} />
			</div>
			<div className='options-container'>
				<DurationCourse onAddDuration={handleDuration} />
				<CourseAuthors
					courseAuthors={courseAuthors}
					onClick={handleDelCourseAuthor}
				/>
			</div>
		</div>
	);
}
