import React, { useCallback } from 'react';
import './CourseAuthors.style.css';
import { Button } from '../../../common/Button';

export function CourseAuthors(props) {
	const handleClick = useCallback(
		(e) => {
			props.onClick(e.target.value);
			// eslint-disable-next-line
	}, [props.onClick]);

	return (
		<div className='options-item'>
			<h2>Course Authors</h2>
			{props.courseAuthors ? (
				props.courseAuthors.map((author) => (
					<div key={author.id} className='author-item'>
						<p>{author.name}</p>
						<div className='author-item-button'>
							<Button value={author.id} onClick={handleClick}>
								Delete Author
							</Button>
						</div>
					</div>
				))
			) : (
				<p className='course-authors-list'>Authors list is empty</p>
			)}
		</div>
	);
}
