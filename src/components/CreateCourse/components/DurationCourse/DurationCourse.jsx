import React, { useCallback, useState } from 'react';
import './DurationCourse.style.css';
import { Input } from '../../../common/Input';

export function DurationCourse(props) {
	const [duration, setDuration] = useState('');

	const handleClick = useCallback((value) => {
		if (!value) {
			// props.onAddDuration(duration);
		}

		setDuration(value);
		props.onAddDuration(value);

		// eslint-disable-next-line
	}, [ ]);

	const getDurationInHours = useCallback(() => {
		if (!duration) {
			return '00:00';
		}
		const hours = Math.floor(duration / 60);
		const minutes = duration % 60;
		return `${hours}:${minutes} hours`;

		// eslint-disable-next-line
	}, [ duration]);

	return (
		<div className='options-item'>
			<h2>Duration</h2>
			<Input
				onChange={handleClick}
				value={duration}
				placeholder='Enter course duration...'
				label='Duration:'
				id='courseDuration'
				type='number'
			/>
			<p>Duration {getDurationInHours()} hours</p>
		</div>
	);
}
