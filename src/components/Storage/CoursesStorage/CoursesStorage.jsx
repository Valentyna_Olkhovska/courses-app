import React from 'react';

import { mockedCoursesList } from '../../constans';

export const CoursesContext = React.createContext({
	courses: [],
	setCourses: () => undefined,
});

export function CoursesStorage({ children }) {
	const [courses, setCourses] = React.useState(mockedCoursesList);

	const context = {
		courses,
		setCourses,
	};

	return (
		<CoursesContext.Provider value={context}>
			{children}
		</CoursesContext.Provider>
	);
}
