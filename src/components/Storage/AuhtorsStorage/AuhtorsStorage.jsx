import React from 'react';

import { mockedAuthorsList } from '../../constans';

export const AuhtorsContext = React.createContext({
	authors: [],
	setAuthors: () => undefined,
});

export function AuhtorsStorage({ children }) {
	const [authors, setAuthors] = React.useState(mockedAuthorsList);

	const context = {
		authors,
		setAuthors,
	};

	return (
		<AuhtorsContext.Provider value={context}>
			{children}
		</AuhtorsContext.Provider>
	);
}
