import React from 'react';
import './Button.style.css';

export function Button({ children, onClick, value }) {
	return (
		<button onClick={onClick} value={value} className='button'>
			{children}
		</button>
	);
}
