import React from 'react';
import './TextArea.style.css';

export function TextArea(props) {
	return (
		<div className='text-area-container'>
			<label htmlFor='search'>{props.label}</label>
			<textarea
				onChange={(e) => {
					props.onChange(e.target.value);
				}}
				value={props.value}
				name={props.name}
				id={props.id}
				placeholder={props.placeholder}
				className='text-area'
				type='text'
				cols='30'
				rows='10'
			/>
		</div>
	);
}
