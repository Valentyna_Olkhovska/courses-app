import React, { useCallback, useState } from 'react';
import './SearchBar.style.css';
import { Button } from '../../../common/Button';
import { Input } from '../../../common/Input';

export function SearchBar(props) {
	const [value, setValue] = useState('');

	const handleClick = useCallback(() => {
		props.onSearch(value);
		// eslint-disable-next-line
	}, [props.onSearch, value]);

	const handleChange = useCallback(
		(nextValue) => {
			if (!nextValue) {
				props.onSearch(nextValue);
			}

			setValue(nextValue);
		}, // eslint-disable-next-line
		[props.onSearch]
	);

	return (
		<div className='searchBar-container'>
			<Input
				onChange={handleChange}
				value={value}
				placeholder='Enter course name or id'
				label='Search courses:'
				id='search'
				type='text'
			/>
			<Button onClick={handleClick}>Search</Button>
		</div>
	);
}
