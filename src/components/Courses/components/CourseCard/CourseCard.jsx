import React from 'react';
import './CourseCard-style.css';
import { Button } from '../../../common/Button';
import { AuhtorsContext } from '../../../Storage/AuhtorsStorage';

export function CourseCard(props) {
	const { authors } = React.useContext(AuhtorsContext);

	const authorsForCourse = React.useMemo(() => {
		const autchorsName = props.course.authors.map((authorId) => {
			const authorCard = authors.find((author) => {
				return author.id === authorId;
			});
			return authorCard.name.toString();
		});
		return autchorsName.toString();
		// eslint-disable-next-line
	}, [authors, props.authors]);

	const duration = React.useMemo(() => {
		const hours = Math.floor(props.course.duration / 60);
		const minutes = props.course.duration % 60;

		return `${hours}:${minutes} hours`;
	}, [props.course.duration]);

	return (
		<div className='cours-item'>
			<div className='cours-info'>
				<h2 className='course-title'>{props.course.title}</h2>
				<p> {props.course.description}</p>
			</div>
			<div className='cours-options'>
				<div className='cours-options-item'>
					<p>Authors:</p>
					<p className='authors-item'>{authorsForCourse}</p>
				</div>
				<div className='cours-options-item'>
					<p>Duration: </p>
					<p>{duration}</p>
				</div>
				<div className='cours-options-item'>
					<p>Created:</p>
					<p> {props.course.creationDate}</p>
				</div>
				<Button>Show course</Button>
			</div>
		</div>
	);
}
