import React, { useCallback } from 'react';
import './Courses.style.css';
import { Button } from '../common/Button';
import { SearchBar } from './components/SearchBar';
import { CourseCard } from './components/CourseCard';
import { CoursesContext } from '../Storage/CoursesStorage';

export function Courses(props) {
	const { courses, setCourses } = React.useContext(CoursesContext);

	const handleSearch = useCallback(
		(value) => {
			if (!value) {
				setCourses(courses);
				return;
			}

			setCourses(
				courses.filter(
					(course) =>
						course.title.toLowerCase().includes(value.toLowerCase()) ||
						course.id.includes(value)
				)
			);
		},
		[courses, setCourses]
	);

	return (
		<div className='courses-container'>
			<div className='courses-container-options'>
				<SearchBar onSearch={handleSearch} />
				<Button onClick={props.onAddNewCourseClick}>Add new course</Button>
			</div>
			<div>
				{courses.map((course) => (
					<CourseCard course={course} key={course.id} />
				))}
			</div>
		</div>
	);
}
