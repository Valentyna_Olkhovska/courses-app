import React from 'react';
import './Header-style.css';
import { Logo } from './components/Logo';
import { Button } from '../common/Button';

export function Header() {
	return (
		<div className='header-container'>
			<Logo />
			<div className='header-options'>
				<h2>my Name</h2>
				<Button>Logout</Button>
			</div>
		</div>
	);
}
