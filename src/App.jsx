import React, { useState } from 'react';
import './App.css';
import { Courses } from './components/Courses';
import { Header } from './components/Header';
import { CreateCourse } from './components/CreateCourse';
import { AuhtorsStorage } from './components/Storage/AuhtorsStorage';
import { CoursesStorage } from './components/Storage/CoursesStorage';

function App() {
	const [isCourseCreation, setCourseCreation] = useState(true);

	const openCreateCourse = () => {
		setCourseCreation(true);
	};

	const closeCreateCourse = () => {
		setCourseCreation(false);
	};

	return (
		<AuhtorsStorage>
			<CoursesStorage>
				<div>
					<Header />
					{isCourseCreation ? (
						<CreateCourse onCloseCourseClick={closeCreateCourse} />
					) : (
						<Courses onAddNewCourseClick={openCreateCourse} />
					)}
				</div>
			</CoursesStorage>
		</AuhtorsStorage>
	);
}
export default App;
